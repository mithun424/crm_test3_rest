package com.conygre.spring.service;

import com.conygre.spring.dal.CrmRepository;
import com.conygre.spring.datatypes.Status;
import com.conygre.spring.entitiy.dto.CommentTest;
import com.conygre.spring.entity.dto.Comment;
import com.conygre.spring.entity.viewmodel.CommentVM;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class CustomerInteractionServiceTest {
    @Autowired
    CustomerInteractionService customerInteractionService;

    @MockBean
    CrmRepository crmdao;

    private ObjectId Id=new ObjectId();

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void getAllComment_Success() {
        List<Comment> comments = new ArrayList<Comment>(Arrays.asList(
                new Comment(Id, UUID.randomUUID(), UUID.randomUUID(),"name","title","description",Status.OPEN,UUID.randomUUID(),"assignee_name",true),
                new Comment(Id, UUID.randomUUID(), UUID.randomUUID(),"name","title","description",Status.OPEN,UUID.randomUUID(),"assignee_name",true)
        ));
        doReturn(comments).when(crmdao).getAllComment();
        List<CommentVM> commentsResult = customerInteractionService.getAllComment();
        assertEquals(comments.size(), commentsResult.size());
    }
    @Test
    public void saveComment_Success(){
        doReturn(true).when(crmdao).saveComment(any(Comment.class));
        CommentVM commentVM=new CommentVM();
        commentVM.setCustomerId(UUID.randomUUID());
        commentVM.setCustomerName("name");
        commentVM.setAssignee(UUID.randomUUID());
        commentVM.setDescription("Description");
        commentVM.setCommentId(UUID.randomUUID());
        commentVM.setAssigneeName("assignee_name");
        commentVM.setStatus(Status.OPEN);
        commentVM.setTitle("title");
       boolean IsSuccess=customerInteractionService.saveComment(commentVM);
        assertEquals(true,IsSuccess);
    }
    @Test
    public void SaveComment_Update(){
        doReturn(true).when(crmdao).updateComment(any(Comment.class));
        CommentVM commentVM=new CommentVM();
        commentVM.setCustomerId(UUID.randomUUID());
        commentVM.setCustomerName("name");
        commentVM.setAssignee(UUID.randomUUID());
        commentVM.setDescription("Description");
        commentVM.setCommentId(UUID.randomUUID());
        commentVM.setAssigneeName("assigned_name");
        commentVM.setStatus(Status.OPEN);
        commentVM.setTitle("title");
        assertEquals(true,customerInteractionService.updateComment(commentVM));
    }

}
