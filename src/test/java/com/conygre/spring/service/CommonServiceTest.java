package com.conygre.spring.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CommonServiceTest {
    private CommonService commonService;

    @BeforeEach
    void setUp() {
        commonService = new CommonService();
    }

    @Test
    void getRoleList_Success(){
        List<String> ls = new ArrayList<>(Arrays.asList("ADMIN", "CUSTOMER"));
        assertEquals(ls, commonService.getRoleList());
    }

    @Test
    void getStatusList_Success(){
        List<String> ls = new ArrayList<>(Arrays.asList("OPEN", "INPROGRESS", "REVIEW", "DONE"));
        assertEquals(ls, commonService.getStatusList());
    }
}
