package com.conygre.spring.service;

import com.conygre.spring.dal.CrmRepository;
import com.conygre.spring.datatypes.Role;
import com.conygre.spring.entity.dto.Customer;
import com.conygre.spring.entity.viewmodel.CustomerVM;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    CustomerService customerService;

    @MockBean
    CrmRepository crmRepository;

    @BeforeEach
    void setUp(){
        initMocks(this);
    }

    @Test
    void getAllCustomer_Success(){
        ObjectId id = new ObjectId();
        List<Customer> customers = new ArrayList<Customer>();
        Customer c = new Customer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), "second", "lastnames", "second@lastname.com", "2222222222", Role.CUSTOMER, "Addresss1", true);
        c.setId(id);
        customers.add(c);
        doReturn(customers).when(crmRepository).getAllCustomer();
        List<CustomerVM> cvList = new ArrayList<>();
        CustomerVM cc = new CustomerVM();
        cc.setActive(true);
        cc.setAddress("Address1");
        cc.setFirstName("second");
        cc.setLastName("lastnames");
        cc.setMobile("2222222222");
        cc.setEmail("second@lastname.com");
        cc.setCustomerId(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"));
        cc.setRole(Role.CUSTOMER);
        cc.setId("c1db4fcd-b702-4865-8daf-dd18a68627b8");
        cvList.add(cc);
        List<CustomerVM> result= customerService.getAllCustomer();
        assertEquals(cvList.size(), result.size());
    }

    @Test
    void saveCustomer_Success(){
        Customer c = new Customer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), "second", "lastnames", "second@lastname.com", "2222222222", Role.CUSTOMER, "Addresss1", true);
        CustomerVM cV = new CustomerVM();
        doReturn(true).when(crmRepository).saveCustomer(any());
        assertEquals(true, customerService.saveCustomer(cV));
    }

    @Test
    void updateCustomer_Success(){
        Customer c = new Customer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), "second", "lastnames", "second@lastname.com", "2222222222", Role.CUSTOMER, "Addresss1", true);
        ObjectId id = new ObjectId();
        c.setId(id);
        doReturn(true).when(crmRepository).updateCustomer(any());
        CustomerVM cc = new CustomerVM();
        cc.setActive(true);
        cc.setAddress("Address1");
        cc.setFirstName("second");
        cc.setLastName("lastnames");
        cc.setMobile("2222222222");
        cc.setEmail("second@lastname.com");
        cc.setCustomerId(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"));
        cc.setRole(Role.CUSTOMER);
        cc.setId(id.toHexString());
        assertEquals(true, customerService.updateCustomer(cc));
    }

    @Test
    void deleteCustomer_Success(){
        Customer c = new Customer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), "second", "lastnames", "second@lastname.com", "2222222222", Role.CUSTOMER, "Addresss1", true);
        doReturn(c).when(crmRepository).getCustomerById(any());
        doReturn(true).when(crmRepository).deleteCustomer(any());
        assertEquals( true, customerService.deleteCustomer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8")));
    }
}
