package com.conygre.spring.dal;

import com.conygre.spring.datatypes.Role;
import com.conygre.spring.datatypes.Status;
import com.conygre.spring.entity.dto.Comment;
import com.conygre.spring.entity.dto.Customer;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class CrmRepositoryTest {
    private CrmRepository repository;
    private ObjectId Id=new ObjectId();

    @Mock
    MongoTemplate mongoTemplate;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void getAllComment_Success() {
        List<Comment> comments = new ArrayList<Comment>(Arrays.asList(
                new Comment(Id, UUID.randomUUID(), UUID.randomUUID(),"name","title","description",Status.OPEN,UUID.randomUUID(),"assignee_name",true),
                new Comment(Id, UUID.randomUUID(), UUID.randomUUID(),"name","title","description",Status.OPEN,UUID.randomUUID(),"assignee_name",true)
                ));
        List<Customer> customers = new ArrayList<Customer>(Arrays.asList(
                new Customer(UUID.randomUUID(),"fname","lname","email","mobile", Role.ADMIN,"address",false)
        ));
        repository = new CrmRepository(mongoTemplate);
        doReturn(comments, customers).when(mongoTemplate).findAll(any());
        List<Comment> commentsResult =  repository.getAllComment();
        assertEquals(comments.size(), commentsResult.size() );
    }
}
