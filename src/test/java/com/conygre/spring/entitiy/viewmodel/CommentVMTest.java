package com.conygre.spring.entitiy.viewmodel;

import com.conygre.spring.datatypes.Status;
import com.conygre.spring.entity.viewmodel.CommentVM;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class CommentVMTest {
    private CommentVM commentVM;

    private UUID Id = UUID.randomUUID();
    @BeforeEach
    void setUp() {
        commentVM = new CommentVM();
        commentVM.setCommentId(Id);
        commentVM.setAssigneeName("M");
        commentVM.setAssignee(UUID.randomUUID());
        commentVM.setCustomerName("M");
        commentVM.setCustomerId(UUID.randomUUID());
        commentVM.setDescription("Desc 1");
        commentVM.setTitle("Title 1");
        commentVM.setStatus(Status.OPEN);


    }

    @Test
    void getAssignee() {
        assertEquals(commentVM.getAssignee(), commentVM.getAssignee());
    }

    @Test
    void getAssigneeName() {
        assertEquals("M", commentVM.getAssigneeName());
    }

    @Test
    void getCustomerName() {
        assertEquals("M", commentVM.getCustomerName());
    }

    @Test
    void getDescription() {
        assertEquals("Desc 1", commentVM.getDescription());
    }

    @Test
    void getTitle() {
        assertEquals("Title 1", commentVM.getTitle());
    }

    @Test
    void getCustomerId() {
        assertEquals(commentVM.getCustomerId(), commentVM.getCustomerId());
    }

    @Test
    void getStatus() {
        assertEquals(Status.OPEN, commentVM.getStatus());
    }
}
