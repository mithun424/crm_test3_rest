package com.conygre.spring.entitiy.viewmodel;

import com.conygre.spring.dal.CrmRepository;
import com.conygre.spring.datatypes.Role;
import com.conygre.spring.entity.viewmodel.CustomerVM;
import com.conygre.spring.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

class CustomerVMTest {

    private CustomerVM customerVM;

    @BeforeEach
    void setUp(){
        customerVM= new CustomerVM();
        customerVM.setId("1");
        customerVM.setCustomerId(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"));
        customerVM.setFirstName("second");
        customerVM.setLastName("lastname1");
        customerVM.setEmail("second@lasternam1.com");
        customerVM.setMobile("2222222222");
        customerVM.setAddress("Address1");
        customerVM.setRole(Role.ADMIN);
        customerVM.setActive(true);
    }

    @Test
    void getId_Success() {
        assertEquals("1", customerVM.getId());
    }

    @Test
    void isActive() {
        assertEquals(true, customerVM.isActive());
    }

    @Test
    void getCustomerId() {
        assertEquals(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), customerVM.getCustomerId());
    }

    @Test
    void getFirstName() {
        assertEquals("second", customerVM.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("lastname1", customerVM.getLastName());
    }

    @Test
    void getEmail() {
        assertEquals("second@lasternam1.com", customerVM.getEmail());
    }

    @Test
    void getMobile() {
        assertEquals("2222222222", customerVM.getMobile());
    }

    @Test
    void getRole() {
        assertEquals(Role.ADMIN, customerVM.getRole());
    }

    @Test
    void getAddress() {
        assertEquals("Address1", customerVM.getAddress());
    }
}