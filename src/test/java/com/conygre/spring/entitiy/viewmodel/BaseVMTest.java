package com.conygre.spring.entitiy.viewmodel;

import com.conygre.spring.entity.viewmodel.BaseVM;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
class BaseVMTest {

    @Mock
    private BaseVM baseVM;

    @BeforeEach
    void setUp(){
    initMocks(this);
    baseVM = new BaseVM();
    baseVM.setErrorMessage("Error Caught");
    baseVM.setSuccess(false);
    }

    @Test
    void isSuccess() {
        assertEquals(false, baseVM.isSuccess());
    }

    @Test
    void getErrorMessage() {
        assertEquals("Error Caught", baseVM.getErrorMessage());
    }
}