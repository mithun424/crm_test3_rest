package com.conygre.spring.entitiy.dto;

import com.conygre.spring.datatypes.Status;
import com.conygre.spring.entity.dto.Comment;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommentTest {
    private Comment comment;
    private ObjectId Id = new ObjectId();
    @BeforeEach
    void setUp() {
        comment =   new Comment(Id, UUID.randomUUID(), UUID.randomUUID(),"name","title","description",Status.OPEN,UUID.randomUUID(),"assignee_name",true);
    }
    @Test
    void getId() {
        assertEquals(Id, comment.getId());
    }

    @Test
    void getAssignee() {
        assertEquals(comment.getAssignee(), comment.getAssignee());
    }

    @Test
    void getAssigneeName() {
        assertEquals("assignee_name", comment.getAssigneeName());
    }

    @Test
    void getCustomerName() {
        assertEquals("name", comment.getCustomerName());
    }

    @Test
    void getDescription() {
        assertEquals("description", comment.getDescription());
    }

    @Test
    void getTitle() {
        assertEquals("title", comment.getTitle());
    }

    @Test
    void getCustomerId() {
        assertEquals(comment.getCustomerId(), comment.getCustomerId());
    }

    @Test
    void getIsActive() {
        assertEquals(true, comment.getIsActive());
    }

    @Test
    void getStatus() {
        assertEquals(Status.OPEN, comment.getStatus());
    }



}
