package com.conygre.spring.entitiy.dto;

import com.conygre.spring.datatypes.Role;
import com.conygre.spring.entity.dto.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CustomerTest {
    private Customer customer2;

    @BeforeEach
    void setUp(){
        customer2 = new Customer(UUID.fromString("c1db4fcd-b702-4865-8daf-dd18a68627b8"), "second", "lastnames", "second@lastname.com", "2222222222", Role.CUSTOMER, "Addresss1", true);

    }

    @Test
    void getAddress() {
        assertEquals("Addresss1", customer2.getAddress());
    }

    @Test
    void getFirstName() {
        assertEquals("second", customer2.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("lastnames", customer2.getLastName());
    }

    @Test
    void getEmail() {
        assertEquals("second@lastname.com", customer2.getEmail());
    }

    @Test
    void getMobile() {
        assertEquals("2222222222", customer2.getMobile());
    }

    @Test
    void getRole() {
        assertEquals(Role.CUSTOMER, customer2.getRole());
    }

    @Test
    void getIsActive() {
        assertEquals(true, customer2.getIsActive());
    }
}