package com.conygre.spring.dal;

import com.conygre.spring.entity.dto.Comment;
import com.conygre.spring.entity.dto.Customer;
import com.conygre.spring.utilties.Utilities;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class CrmRepository implements  ICrmRepository {

    private MongoTemplate mongoTemplate;
    private Query query;

    public CrmRepository(@Autowired MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    @Transactional
    public List<Customer> getAllCustomer() {
        List<Customer> customers=new ArrayList();
        try{
            query = new Query();
            customers = mongoTemplate.findAll(Customer.class);
            /*if(customers!=null && customers.stream().count()>0) {
                customers = customers.stream().filter(customer -> customer.getIsActive() == true).collect(Collectors.toList());
            }*/
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return customers;
    }

    @Override
    @Transactional
    public Customer getCustomerById(UUID customerId) {
        Customer customer=new Customer();
        try{
            query = new Query();
            query.addCriteria(Criteria.where("customerId").is(customerId));
            // query.addCriteria(Criteria.where("isActive").is(true));
            customer = mongoTemplate.findOne(query, Customer.class);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return customer;
    }

    @Override
    @Transactional
    public boolean saveCustomer(Customer customer) {
        Customer status=new Customer();
       customer.setCustomerId(UUID.randomUUID());
        try{
            status = mongoTemplate.save(customer);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return status!=null?true:false;
    }

    @Override
    @Transactional
    public boolean updateCustomer(Customer customer) {
        Update update = new Update();
        customer.setIsActive(true);
        update.set("firstName", customer.getFirstName());
        update.set("lastName", customer.getLastName());
        update.set("email", customer.getEmail());
        update.set("mobile", customer.getMobile());
        update.set("role", customer.getRole());
        update.set("isActive", customer.getIsActive());
        update.set("customerId", customer.getCustomerId());
        update.set("address", customer.getAddress());
        try {
            query = new Query();
            query.addCriteria(Criteria.where("_id").is(customer.getId()));
            UpdateResult updateResult = mongoTemplate.updateFirst(query,update, Customer.class);
            return updateResult.getMatchedCount()>0 ? true : false;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
      return  true;
    }

    @Override
    @Transactional
    public boolean deleteCustomer(Customer customer) {
        Update update = new Update();
        update.set("firstName", customer.getFirstName());
        update.set("lastName", customer.getLastName());
        update.set("email", customer.getEmail());
        update.set("mobile", customer.getMobile());
        update.set("role", customer.getRole());
        update.set("isActive", customer.getIsActive() ? false : true);
        update.set("customerId", customer.getCustomerId());
        try{
            query = new Query();
            query.addCriteria(Criteria.where("id").is(customer.getId()));
            UpdateResult updateResult = mongoTemplate.updateFirst(query,update, Customer.class);
            List<Comment> comments =  getCommentByCustomerId(customer.getCustomerId());
            for (Comment comment:comments) {
                deleteComment(comment);
            }
            return updateResult.getMatchedCount()>0 ? true : false;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public List<Comment> getAllComment() {
        List<Comment> comments = new ArrayList();
        try{
            query = new Query();
            comments = mongoTemplate.findAll(Comment.class);
            List<Customer> customers = mongoTemplate.findAll(Customer.class);
            if(comments != null && customers != null) {
                comments.forEach( comment -> {
                    Customer customerObj = customers.stream().filter((customer) -> customer.getCustomerId().equals(comment.getCustomerId()))
                            .findAny().orElse(new Customer());
                    Customer assignObj = customers.stream().filter((customer) -> customer.getCustomerId().equals(comment.getAssignee()))
                            .findAny().orElse(new Customer());
                    comment.setCustomerName(String.format("%s %s",customerObj.getFirstName(), customerObj.getLastName()));
                    comment.setAssigneeName(String.format("%s %s",assignObj.getFirstName(), assignObj.getLastName()));
                });
            }
            /* if(comments!=null && comments.stream().count()>0) {
                comments = comments.stream().filter(comment -> comment.getIsActive() == true).collect(Collectors.toList());
            }*/
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return comments;
    }

    @Override
    @Transactional
    public Comment getCommentByCommentId(int commentId) {
        Comment comment=new Comment();
        try{
            query = new Query();
            query.addCriteria(Criteria.where("commentId").is(commentId));
            query.addCriteria(Criteria.where("isActive").is(true));
            comment = mongoTemplate.findOne(query, Comment.class);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return comment;
    }

    @Override
    @Transactional
    public List<Comment> getCommentByCustomerId(UUID customerId) {
        List<Comment> comments=new ArrayList();
        try{
            query = new Query();
            query.addCriteria(Criteria.where("customerId").is(customerId));
            //query.addCriteria(Criteria.where("isActive").is(true));
            comments = mongoTemplate.find(query, Comment.class);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return comments;
    }

    @Override
    @Transactional
    public boolean saveComment(Comment comment) {
        Comment status=new Comment();
        comment.setCommentId(UUID.randomUUID());
        comment.setIsActive(true);
        try{
            status  = mongoTemplate.save(comment);
            return status!=null?true:false;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean updateComment(Comment comment) {
        Update update = new Update();
        comment.setIsActive(true);
        update.set("title", comment.getTitle());
        update.set("description", comment.getDescription());
        update.set("status", comment.getStatus());
        update.set("assignee", comment.getAssignee());
        update.set("customerId", comment.getCustomerId());
        update.set("isActive", comment.getIsActive());
        try{
            query = new Query();
            query.addCriteria(Criteria.where("commentId").is(comment.getCommentId()));
            UpdateResult updateResult = mongoTemplate.updateFirst(query,update, Comment.class);
            return updateResult.getMatchedCount()>0 ? true : false;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
       return true;
    }

    @Override
    @Transactional
    public boolean deleteComment(Comment comment) {
        Update update = new Update();
        comment.setIsActive(false);
        update.set("customerId", comment.getCustomerId());
        update.set("tittle", comment.getTitle());
        update.set("description", comment.getDescription());
        update.set("status", comment.getStatus());
        update.set("assignee", comment.getAssignee());
        update.set("isActive", comment.getIsActive()? false : true);
        try{
            query = new Query();
            query.addCriteria(Criteria.where("commentId").is(comment.getId()));
            UpdateResult updateResult = mongoTemplate.updateFirst(query,update, Comment.class);
            return updateResult.getMatchedCount()>0 ? true : false;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
       return true;
    }

}
