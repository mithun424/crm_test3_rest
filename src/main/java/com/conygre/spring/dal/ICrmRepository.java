package com.conygre.spring.dal;

import com.conygre.spring.entity.dto.Comment;
import com.conygre.spring.entity.dto.Customer;

import java.util.List;
import java.util.UUID;

public interface ICrmRepository {
    public List<Customer> getAllCustomer();
    public Customer getCustomerById(UUID customerId);
    public boolean saveCustomer(Customer customer);
    public  boolean updateCustomer(Customer customer);
    public  boolean deleteCustomer(Customer customer);
    public List<Comment> getAllComment();
    public Comment getCommentByCommentId(int commentId);
    public List<Comment> getCommentByCustomerId(UUID customerId);
    public boolean saveComment(Comment comment);
    public  boolean updateComment(Comment comment);
    public  boolean deleteComment(Comment comment);

}
