package com.conygre.spring.entity.viewmodel;

import com.conygre.spring.datatypes.Status;

public class ChartVM  {
    private Status status;
    private int TotalComments;
    private String style;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getTotalComments() {
        return TotalComments;
    }

    public void setTotalComments(int totalComments) {
        TotalComments = totalComments;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
