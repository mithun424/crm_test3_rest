package com.conygre.spring.entity.viewmodel;

import com.conygre.spring.datatypes.Status;
import org.bson.types.ObjectId;

import java.util.UUID;

public class CommentVM {
    private UUID commentId;
    private UUID customerId; // TODO: ObjectId
    private String customerName;
    private String title;
    private String description;
    private Status status;
    private UUID assignee; // TODO: ObjectId
    private String assigneeName;

    public String getCustomerName() {
        return customerName;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UUID getAssignee() {
        return assignee;
    }

    public void setAssignee(UUID assignee) {
        this.assignee = assignee;
    }
}
