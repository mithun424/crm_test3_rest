package com.conygre.spring.entity.viewmodel;

import java.util.List;

public class CustomerRelationVm extends BaseVM {
    private CustomerVM customer;
    private List<CustomerVM> customers;
    private CommentVM comment;
    private List<CommentVM> comments;
    private List<String> roles;
    private List<String> statusList;
    private  List<ChartVM> chartVMList;

    public List<ChartVM> getChartVMList() {
        return chartVMList;
    }

    public void setChartVMList(List<ChartVM> chartVMList) {
        this.chartVMList = chartVMList;
    }

    public CustomerVM getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerVM customer) {
        this.customer = customer;
    }

    public List<CustomerVM> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerVM> customers) {
        this.customers = customers;
    }

    public CommentVM getComment() {
        return comment;
    }

    public void setComment(CommentVM comment) {
        this.comment = comment;
    }

    public List<CommentVM> getComments() {
        return comments;
    }

    public void setComments(List<CommentVM> comments) {
        this.comments = comments;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }
}
