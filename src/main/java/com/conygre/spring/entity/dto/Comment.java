package com.conygre.spring.entity.dto;

import com.conygre.spring.datatypes.Status;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
public class Comment {

    @Id
    private ObjectId Id;
    private UUID commentId;
    private UUID customerId; // TODO: ObjectId
    private String customerName;
    private String title;
    private String description;
    private Status status;
    private  UUID assignee; // TODO:  ObjectId
    private String assigneeName;
    private  boolean isActive;

    public Comment() {
    }

    public Comment(ObjectId Id, UUID commentId, UUID customerId, String customerName, String title, String description, Status status, UUID assignee, String assigneeName, boolean isActive) {
        this.Id = Id;
        this.commentId = commentId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.title = title;
        this.description = description;
        this.status = status;
        this.assignee = assignee;
        this.assigneeName = assigneeName;
        this.isActive = isActive;
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public ObjectId getId() {
        return Id;
    }

    public void setId(ObjectId id) {
        this.Id = id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UUID getAssignee() {
        return assignee;
    }

    public void setAssignee(UUID assignee) {
        this.assignee = assignee;
    }
}
