package com.conygre.spring.service;

import com.conygre.spring.dal.CrmRepository;
import com.conygre.spring.datatypes.Role;
import com.conygre.spring.entity.dto.Customer;
import com.conygre.spring.entity.viewmodel.CustomerVM;
import com.conygre.spring.exceptionhandler.CustomException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CustomerService implements ICustomerService {
    @Autowired
    CrmRepository crmRepository;


    @Override
    public List<CustomerVM> getAllCustomer() {
        List<CustomerVM> customerVMList = new ArrayList<>();
        List<Customer> customers = crmRepository.getAllCustomer();
        if(customers!= null && customers.stream().count()>0) {
            for (Customer customer : customers) {
                CustomerVM customerVM = new CustomerVM();
                customerVM.setId(customer.getId().toHexString());
                customerVM.setCustomerId(customer.getCustomerId());
                customerVM.setEmail(customer.getEmail());
                customerVM.setFirstName(customer.getFirstName());
                customerVM.setLastName(customer.getLastName());
                customerVM.setMobile(customer.getMobile());
                customerVM.setRole(customer.getRole());
                customerVM.setAddress(customer.getAddress());
                customerVM.setActive(customer.getIsActive());
                customerVMList.add(customerVM);
            }
        }
        else
            new CustomException("Custome not found");
        return customerVMList;
    }

    @Override
    public CustomerVM getCustomerById(UUID customerId) {
        CustomerVM customerVM = new CustomerVM();
        Customer customer = crmRepository.getCustomerById(customerId);
        if(customer!= null) {
            customerVM.setCustomerId(customer.getCustomerId());
            customerVM.setEmail(customer.getEmail());
            customerVM.setFirstName(customer.getFirstName());
            customerVM.setLastName(customer.getLastName());
            customerVM.setMobile(customer.getMobile());
          //  customerVM.setRole(Role.valueOf(customer.getRole()));
        }
        else
             new CustomException("Customer not found with Id"+ customerId);
        return  customerVM;
    }

    @Override
    public boolean saveCustomer(CustomerVM customerVM) {
        Customer customer = new Customer();
        //customer.setCustomerId(UUID.randomUUID());
        customer.setEmail(customerVM.getEmail());
        customer.setFirstName(customerVM.getFirstName());
        customer.setLastName(customerVM.getLastName());
        customer.setMobile(customerVM.getMobile());
        customer.setIsActive(true);
        customer.setRole(customerVM.getRole());
        customer.setAddress(customerVM.getAddress());
        return crmRepository.saveCustomer(customer);
    }

    @Override
    public boolean updateCustomer(CustomerVM customerVM) {
        Customer customer = new Customer();

        customer.setId(new ObjectId(customerVM.getId()));
        customer.setCustomerId(customerVM.getCustomerId());
        customer.setEmail(customerVM.getEmail());
        customer.setFirstName(customerVM.getFirstName());
        customer.setLastName(customerVM.getLastName());
        customer.setMobile(customerVM.getMobile());
        customer.setIsActive(true);
        customer.setRole(customerVM.getRole());
        customer.setAddress(customerVM.getAddress());
        return crmRepository.updateCustomer(customer);
    }

    @Override
    public boolean deleteCustomer(UUID  customerId) {
        Customer customer = crmRepository.getCustomerById(customerId);
        return crmRepository.deleteCustomer(customer);
    }
}
