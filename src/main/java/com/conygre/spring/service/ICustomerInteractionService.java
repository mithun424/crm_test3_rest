package com.conygre.spring.service;

import com.conygre.spring.entity.viewmodel.ChartVM;
import com.conygre.spring.entity.viewmodel.CommentVM;

import java.util.List;
import java.util.UUID;

public interface ICustomerInteractionService {
    public List<CommentVM> getAllComment();
    public CommentVM getCommentByCommentId(int commentId);
    public List<CommentVM> getCommentByCustomerId(UUID customerId);
    public boolean saveComment(CommentVM comment);
    public  boolean updateComment(CommentVM comment);
    public  boolean deleteComment(int commentId);
    public  List<ChartVM> chartData();
}
