package com.conygre.spring.service;

import java.util.List;

public interface ICommonService {
   public List<String> getRoleList();
   public List<String> getStatusList();
}
