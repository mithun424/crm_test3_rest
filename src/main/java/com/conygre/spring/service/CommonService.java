package com.conygre.spring.service;

import com.conygre.spring.datatypes.Role;
import com.conygre.spring.datatypes.Status;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommonService implements  ICommonService {


    @Override
    public List<String> getRoleList() {
        List<String>  roles = new ArrayList<>();
        for (Role role: Role.values()){
             roles.add(role.toString());
        }
        return roles;
    }

    @Override
    public List<String> getStatusList() {
        List<String>  statusList = new ArrayList<>();
        for (Status status: Status.values()){
            statusList.add(status.toString());
        }
        return statusList;
    }
}
