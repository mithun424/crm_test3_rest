package com.conygre.spring.service;

import com.conygre.spring.dal.CrmRepository;
import com.conygre.spring.datatypes.Role;
import com.conygre.spring.datatypes.Status;
import com.conygre.spring.entity.dto.Comment;
import com.conygre.spring.entity.viewmodel.ChartVM;
import com.conygre.spring.entity.viewmodel.CommentVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerInteractionService implements  ICustomerInteractionService {
    @Autowired
    CrmRepository crmRepository;

    @Override
    public List<CommentVM> getAllComment() {
       List<CommentVM> commentVMList  = new ArrayList<>();
        List<Comment> comments =  crmRepository.getAllComment();
        if(comments!= null && comments.stream().count()>0) {
            for (Comment comment : comments) {
                commentVMList.add(setCommentVMObject(comment));
            }
        }
        return  commentVMList;
    }

    @Override
    public CommentVM getCommentByCommentId(int commentId) {
        CommentVM commentVM =null;
        Comment comment = crmRepository.getCommentByCommentId(commentId);
        if(comment!=null){
            commentVM = setCommentVMObject(comment);
        }
        return commentVM;
    }

    @Override
    public List<CommentVM> getCommentByCustomerId(UUID customerId) {
        List<CommentVM> commentVMList  = new ArrayList<>();
        List<Comment> comments = crmRepository.getCommentByCustomerId(customerId);
        if(comments!=null && comments.stream().count()>0) {
            for (Comment comment : comments) {
                commentVMList.add(setCommentVMObject(comment));
            }
        }
        return  commentVMList;

    }

    @Override
    public boolean saveComment(CommentVM commentVM) {
        Comment comment = new Comment();
        comment.setAssignee(commentVM.getAssignee());
        comment.setCustomerId(commentVM.getCustomerId());
        comment.setTitle(commentVM.getTitle());
        comment.setDescription(commentVM.getDescription());
        comment.setStatus(commentVM.getStatus());
        return crmRepository.saveComment(comment);
    }

    @Override
    public boolean updateComment(CommentVM commentVM) {
        Comment comment = new Comment();
        comment.setCommentId(commentVM.getCommentId());
        comment.setAssignee(commentVM.getAssignee());
        comment.setCustomerId(commentVM.getCustomerId());
        comment.setTitle(commentVM.getTitle());
        comment.setDescription(commentVM.getDescription());
        comment.setStatus(commentVM.getStatus());
        return crmRepository.updateComment(comment);
    }

    @Override
    public boolean deleteComment(int commentId) {
        Comment comment = crmRepository.getCommentByCommentId(commentId);
        return crmRepository.deleteComment(comment);
    }

    private CommentVM setCommentVMObject( Comment comment){
        CommentVM commentVM = new CommentVM();
        commentVM.setCommentId(comment.getCommentId());
        commentVM.setCustomerName(comment.getCustomerName());
        commentVM.setAssigneeName(comment.getAssigneeName());
        commentVM.setAssignee(comment.getAssignee());
        commentVM.setCustomerId(comment.getCustomerId());
        commentVM.setTitle(comment.getTitle());
        commentVM.setDescription(comment.getDescription());
        commentVM.setStatus(comment.getStatus());
        return  commentVM;
    }

    public   List<ChartVM> chartData(){
        List<ChartVM> chartVMS = new ArrayList<>();
        List<Comment> comments =  crmRepository.getAllComment();
        for (Status status: Status.values()){
            ChartVM chartVM = new ChartVM();
            chartVM.setStatus(status);
            if(comments.stream().filter(comment -> comment.getStatus()== status).count()>0) {
               chartVM.setTotalComments((int) comments.stream().filter(comment -> comment.getStatus() == status).collect(Collectors.toList()).stream().count());
            }
            if(status.equals(Status.OPEN))
                chartVM.setStyle("color: #76A7FA");
            if(status.equals(Status.INPROGRESS))
                chartVM.setStyle("color: blue");
            if(status.equals(Status.REVIEW))
                chartVM.setStyle("color: gray");
            if(status.equals(Status.DONE))
                chartVM.setStyle("stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF");
            chartVMS.add(chartVM);
        }

        return  chartVMS;
    }
}
