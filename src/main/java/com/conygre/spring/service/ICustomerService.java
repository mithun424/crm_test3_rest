package com.conygre.spring.service;

import com.conygre.spring.entity.viewmodel.CustomerVM;

import java.util.List;
import java.util.UUID;

public interface ICustomerService {
    public List<CustomerVM> getAllCustomer();
    public CustomerVM getCustomerById(UUID customerId);
    public boolean saveCustomer(CustomerVM customer);
    public  boolean updateCustomer(CustomerVM customer);
    public  boolean deleteCustomer(UUID customerId);
}
