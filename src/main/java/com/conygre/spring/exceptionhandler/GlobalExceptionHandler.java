package com.conygre.spring.exceptionhandler;

import com.conygre.spring.entity.viewmodel.BaseVM;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice("com.conygre.spring")
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<?> handleCustomException(CustomException exception, WebRequest webRequest){
        BaseVM baseVM =  new BaseVM();
        baseVM.setSuccess(false);
        baseVM.setErrorMessage(exception.getMessage());
        return  new ResponseEntity(baseVM, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleGlobalException(Exception exception, WebRequest webRequest){
        BaseVM baseVM =  new BaseVM();
        baseVM.setSuccess(false);
        baseVM.setErrorMessage(exception.getMessage());
        return  new ResponseEntity(baseVM, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
