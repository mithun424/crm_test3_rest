package com.conygre.spring.exceptionhandler;

public class CustomException extends RuntimeException {

    private static final long serialVersionUID =0;
    public CustomException(String message) {
        super(message);
    }
}
