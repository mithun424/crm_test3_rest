package com.conygre.spring.datatypes;

public enum Status {
    OPEN,
    INPROGRESS,
    REVIEW,
    DONE
}
