package com.conygre.spring.datatypes;

public enum Role {
    ADMIN,
    CUSTOMER
}
