package com.conygre.spring.rest;

import com.conygre.spring.entity.viewmodel.CustomerRelationVm;
import com.conygre.spring.service.ICommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/common")
public class CommonController {
    @Autowired
    ICommonService iCommonService;


    @RequestMapping(value = "roles",  method = RequestMethod.GET)
    public CustomerRelationVm getRoles(){
        CustomerRelationVm customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setRoles(iCommonService.getRoleList());
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }
    @RequestMapping(value = "status", method = RequestMethod.GET)
    public CustomerRelationVm getAllCommentStatus(){
        CustomerRelationVm customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setStatusList(iCommonService.getStatusList());
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }

}
