package com.conygre.spring.rest;

import com.conygre.spring.entity.viewmodel.BaseVM;
import com.conygre.spring.entity.viewmodel.CustomerRelationVm;
import com.conygre.spring.entity.viewmodel.CustomerVM;
import com.conygre.spring.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    ICustomerService iCustomerService;

    CustomerRelationVm customerRelationVm = null;

    @RequestMapping(value = "all", method = RequestMethod.GET)
     public CustomerRelationVm getAllCustomer(){
        customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setCustomers(iCustomerService.getAllCustomer());
        customerRelationVm.setSuccess(true);
        return  customerRelationVm;
     }

    @RequestMapping(value = "find/{customerid}", method = RequestMethod.GET)
     public  CustomerRelationVm getCustomerById(@PathVariable("customerid") UUID customerId) {
        customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setCustomer(iCustomerService.getCustomerById(customerId));
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }

     @RequestMapping(value = "save", method = RequestMethod.POST)
     public BaseVM saveCustomer(@RequestBody CustomerVM customerVM){
         BaseVM baseVM = new BaseVM();
         baseVM.setSuccess(iCustomerService.saveCustomer(customerVM));
         return baseVM;
     }

     @RequestMapping(value = "update", method = RequestMethod.PUT)
     public BaseVM updateCustomer(@RequestBody CustomerVM customerVM){
         BaseVM baseVM = new BaseVM();
         baseVM.setSuccess(iCustomerService.updateCustomer(customerVM));
         return  baseVM;
     }

     @RequestMapping(value = "delete", method = RequestMethod.PUT)
     public BaseVM deleteCustomer(@RequestBody CustomerVM customerVM){
         BaseVM baseVM = new BaseVM();
         baseVM.setSuccess(iCustomerService.deleteCustomer(customerVM.getCustomerId()));
         return baseVM;
     }
}
