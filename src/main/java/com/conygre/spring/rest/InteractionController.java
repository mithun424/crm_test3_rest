package com.conygre.spring.rest;

import com.conygre.spring.entity.viewmodel.BaseVM;
import com.conygre.spring.entity.viewmodel.ChartVM;
import com.conygre.spring.entity.viewmodel.CommentVM;
import com.conygre.spring.entity.viewmodel.CustomerRelationVm;
import com.conygre.spring.service.ICustomerInteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("/api/comment")
public class InteractionController {
    @Autowired
    ICustomerInteractionService iCustomerInteractionService;

    CustomerRelationVm customerRelationVm = null;

    @RequestMapping(value = "all",method = RequestMethod.GET)
    public CustomerRelationVm getAllComments(){
        customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setComments(iCustomerInteractionService.getAllComment());
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }
/*
    @RequestMapping(value = "find/{commentId}",method = RequestMethod.GET)
    public CustomerRelationVm  getCommentByCommentId(@PathVariable("commentId") int commentId){
        customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setComment(iCustomerInteractionService.getCommentByCommentId(commentId));
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }

    @RequestMapping(value = "find/customer/{customerId}",method = RequestMethod.GET)
    public CustomerRelationVm  getCommentByCustomerId(@PathVariable("customerId") int customerId){
        customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setComments(iCustomerInteractionService.getCommentByCustomerId(customerId));
        customerRelationVm.setSuccess(true);
        return customerRelationVm;
    }
*/
    @RequestMapping(value = "save",method = RequestMethod.POST)
    public BaseVM saveComment(@RequestBody CommentVM commentVM){
        BaseVM baseVM = new BaseVM();
        baseVM.setSuccess(iCustomerInteractionService.saveComment(commentVM));
        baseVM.setSuccess(true);
        return  baseVM;
    }

    @RequestMapping(value = "update",method = RequestMethod.PUT)
    public  BaseVM updateComment(@RequestBody CommentVM commentVM){
        BaseVM baseVM = new BaseVM();
        baseVM.setSuccess(iCustomerInteractionService.updateComment(commentVM));
        baseVM.setSuccess(true);
        return  baseVM;
    }

    @RequestMapping(value = "delete",method = RequestMethod.DELETE)
    public  BaseVM deleteComment( @RequestBody int commentId){
        BaseVM baseVM = new BaseVM();
        baseVM.setSuccess(iCustomerInteractionService.deleteComment(commentId));
        baseVM.setSuccess(true);
        return  baseVM;
    }

    @RequestMapping(value = "chart",method = RequestMethod.GET)
    public CustomerRelationVm deleteComment(){
        CustomerRelationVm customerRelationVm = new CustomerRelationVm();
        customerRelationVm.setChartVMList(iCustomerInteractionService.chartData());
        customerRelationVm.setSuccess(true);
        return  customerRelationVm;
    }

}
